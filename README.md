index  
http://newcomer2016.dev.so-ra.co.jp/yamaguchi/develop-test/  


メールアドレス確認機能  
http://newcomer2016.dev.so-ra.co.jp/yamaguchi/develop-test/mail_input.html  

- メールアドレスを入力  
- 確認用メールアドレスを入力
- 正しいメールアドレスかを確認したく、@が入っているかを判断し、入っていないとアラートを出す。
- 二つのアドレスが同じかを判断し、違うとアラートをだす。


予約状況確認  
http://newcomer2016.dev.so-ra.co.jp/yamaguchi/develop-test/room_reservation.html


- 3つの部屋の予約状況をJSONファイルを使って、”yes”と”no”で判断する。
- 予約確認を押すと予約状況を表示。予約済みは赤、空いて入る場合は、緑。
- 「予約をする」「予約をキャンセル」で、JSONファイルを書き換え、リアルタイムで予約状況を変更する。
