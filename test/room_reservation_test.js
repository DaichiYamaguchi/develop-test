var assert = chai.assert;
var expect = chai.expect;
var should = chai.should;

  it("JSONが読み込まれている")
  it("値が定義されている")
  it("値の切り替えができている")
  it("値と表示が同じ")

describe('非同期テスト', function(){
  it("JSONが読み込まれている", function(done){
    this.timeout(2000);
    $.ajax({type: "POST", url: 'data.json', dataType: 'json'})
    .done(function(data){
      $(data).each(function(){
        console.log(this.id, this.reserved);
        assert.ok(data);
  		});
  	})
    done();
  });

  it("値が定義されている", function(done){
    var room = new Array();
    var i = 0;
    this.timeout(2000);
    $.ajax({type: "POST", url: 'data.json', dataType: 'json'})
    .done(function(data){
      $(data).each(function(){
        console.log(this.id, this.reserved);
        room.push(this.id);
        assert.ok(room[i]);
        assert.equal(this.id, room[i], 'パラメーターが配列に代入されている');
        i++;
      });
      assert.ok(room);
    })
    done();
  });
  it("値が変更されている", function(done){
    var after_change = JSON.parse('{"id": "large-room", "reserved": "no"}');

    $.ajax({type: "POST", url: 'data.json', dataType: 'json'})
    .done(function(data){
      //処理を書く予定。（できなかった。）
    });
  });
});
