var assert = chai.assert;

describe('メールアドレスの確認', function(){
  it('メールアドレスの@がついてるか', function(){
    assert.strictEqual(cheack_address('example@example.com'), true, '@がついている');
    assert.strictEqual(cheack_address('example.com'), false, '@がついていない');
  });
  it('確認用のメールアドレスがあっているか', function(){
    assert.strictEqual(confirmMainaddress('example@example.com', 'example@example.com'), true, '正しいかつ同じ');
    assert.strictEqual(confirmMainaddress('example@example.com', 'another@another.com'), false, '正しいが同じではない');
    assert.strictEqual(confirmMainaddress('example.com', 'example.com'), false, '正しくないが同じ');
    assert.strictEqual(confirmMainaddress('example.com', 'another.com'), false, '正しくないかつ同じではない');
  });
});
