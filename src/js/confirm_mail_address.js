//メールアドレス確認機能
function cheack_address(value){
  if(value.indexOf('@') === -1 ){
    document.getElementById('output').textContent = '正しいメールアドレスを入力してください。';
    return false;
  }else{
    return true;
  };
};

function confirmMainaddress(address1, address2){
  var cheack_address1 = cheack_address(address1);

  if(cheack_address1 === true){
    var cheack_address2 = cheack_address(address2);
  };

  if(cheack_address1 === true && cheack_address2 === true){
    if(address1 === address2){
      document.getElementById('output').textContent = 'メールアドレスが一致しました。';
      return true;
    }else {
      document.getElementById('output').textContent = '2つのメールアドレスが合っていません。';
      return false;
    };
  }else{
    return false;
  }
}

function mailaddressInput(){
  document.getElementById('form').onsubmit = function() {
    var address1 = document.getElementById('form').address1.value;
    var address2 = document.getElementById('form').address2.value;
    confirmMainaddress(address1, address2);
    return false;
  };
}
mailaddressInput();
