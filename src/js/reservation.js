$(function(){

  $.ajax({url: 'data.json', dataType: 'json'})
  .done(function(data){
    $(data).each(function(){
      if(this.reserved === "yes"){
        var roomName = '#' + this.id;
        $(roomName).find('.check').addClass('reserved');
      }
		});
	})

  $(".change").on('click', function(){

    var number = this.id;
    $.post("data.json", number, function(returnedData) {
    if(returnedData[number].reserved === "yes"){
      $.extend(returnedData[number], JSON.parse('{"reserved": "no"}'));
      console.log(JSON.stringify(returnedData[number]));
    }else{
      $.extend(returnedData[number], JSON.parse('{"reserved": "yes"}'));
      console.log(JSON.stringify(returnedData[number]));
    }
    })
  });

  $('.check').on('click', function(){
    if($(this).hasClass('reserved')){
      $(this).text('予約済み').addClass('red');
    }else {
      $(this).text('空いてます').addClass('green');
    }
  });

});
